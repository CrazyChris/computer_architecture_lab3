# Лабораторная работа 3

- Вахрушев Данил Сергеевич
- Вариант: `lisp` | `risc` | `harv` | `hw` | `instr` | `struct` | `stream` | `mem` | `prob1`

## Язык программирования

Типизация: статическая
- Синтаксис построен в виде S-выражений в инфиксной нотации
- Имеется поддержка переменных типов `int`(целое число), `string`(нуль-терминированная строка) и `char`(символ)
- Все переменные имеют глобальную область видимости и создаются с помощью функции `(setq name value)`
- Доступен цикл `(for ((var init_val update_val)) (exit condition) (loop_body))`
- Функция `print` может вывести только положительные числа, строки или символы
- Функция `read` считывает символ из входного буфера
- Есть поддержка ветвлений вида `(if (condition) (then) (else))`
- Поддерживаются арифметические операции +(бинарный плюс), -(бинарный минус), *(умножение), div(целочисленное деление), mod(взятие остатка от деления)
- Поддерживаются логические операции OR, >, <, =, >=
- Аргументами арифметических и логических операций могут быть только числа или выражения, возвращающие числа

```    
    s_expression ::= atomic_symbol | list
    list ::= "(" s_expression {s_expression} ")"
    atomic_symbol ::= letter atom_part 
    atom_part ::= empty | operation | letter atom_part | number atom_part 
    number ::= "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
    letter ::= "a" | "b" | "c" | ... | "z"
    empty ::= ""
    operation ::= "+" | "-" | "*" | "/"
```

Пример программы:
```
(setq res 0)
(do ((cnt 0 (+ cnt 1)))
    (>= cnt 1000)
    (if (or (= (mod cnt 3) 0) (= (mod cnt 5) 0))
    (setq res (+ res cnt))))
(print res)
```

#### Input program
```
(setq tmp 0)
(do ((a 0 (+ a 1))
     (b 0 (+ b 2)))
    (>= tmp 20)
    ((setq tmp (+ a b))
    (print tmp)
    (print " ")
    (print (+ 5 (+ a b)))
    (print " "))
)
```
#### Output
```
53 8 6 11 9 4 1115 20 18 23 21 26
```


## Организация памяти:
Память для команд и данных раздельная.

Модель памяти процессора:
- Память команд. Машинное слово -- не определено. Реализовано через список словарей.
- Память данных. Машинное слово -- 4 байта, знаковое.

Всего в памяти данных может лежать -- 100 машинных слов

В процессоре расположены 10 регистров общего назначений `r0`, ..., `r10`, а так же указатель на стек(`sp`).

Адреса расположения переменных определяются во время трансляции линейно от начала адресного пространства.
При этом первые 2 адреса `0x0` и `0x1` отведены на работу с ВУ.

## Система команд
#### Регистры
Архитектура построена по принципу load store.

Машинное слово - 32 бита.

- Память данных:
  - Регистры:
    - Регистры общего назначение - `r0`, `r1`, ..., `r10`
    - Указатель на стек - `sp`
    - Счетчик команд - `pc`
    - Регистр адреса - `data_address`
    - Флаг состояния `Z` на выходе из АЛУ
  - адресуется через регистр data_address, который задается
    - прямым значением
    - значением из регистра
  - может быть записана из любого регистра общего назначения
  - может быть записана в любой регистр общего назначения
  - Первые 2 ячейки занимает маппинг ВУ
    - 0x0 - ввод
    - 0x1 - вывод
### Набор инструкций
| Синтаксис   | Такты | Комментарий               |
|:------------|:------|:--------------------------|
| LOAD r1 r2  | 2     | mem[r2] -> r1             | 
| LDA r1 adr  | 2     | mem[adr] -> r1            | 
| LDI r1 num  | 1     | num -> r1                 |
| STORE r1 r2 | 2     | r2 -> mem[r1]             |
| STA adr r1  | 2     | r1 -> mem[adr]            |
| MOV r1 r2   | 1     | r2 -> r1                  |
| ADDI r1 num | 1     | r1 + num -> r1            |
| CMPI r1 num | 1     | r1 - num -> Z             |
| ADD r1 r2   | 1     | r1 + r2 -> r1             |
| SUB r1 r2   | 1     | r1 - r2 -> r1             |
| MUL r1 r2   | 1     | r1 * r2 -> r1             |
| DIV r1 r2   | 1     | r1 // r2 -> r1            |
| MOD r1 r2   | 1     | r1 % r2 -> r1             |
| OR r1 r2    | 1     | r1 or r2 -> r1            |
| EQ r1 r2    | 1     | r1 == r2 -> r1            |
| GT r1 r2    | 1     | r1 > r2 -> r1             |
| LT r1 r2    | 1     | r1 < r2 -> r1             |
| GE r1 r2    | 1     | r1 >= r2 -> r1            |
| JMP adr     | 1     | adr -> pc                 |
| JZ adr      | 1     | if (Z = 1) then adr -> pc |
| JNZ adr     | 1     | if (Z = 0) then adr -> pc |
| HLT adr     | 1     | Stop                      |

Устройства вводы-вывода мапятся на адреса:
- 0x0 -- ввод
- 0x1 -- вывод

Сам ввод-вывод осуществляется посредством записи или чтения данных из соответсвующей ячейки.

### Кодирование инструкций
- Машинный код представлен в виде JSON списка, содержащегося в секции `text` скомпилированной программы
- Одна инструкция - словарь, содержащий код команды и её аргументы
- Индекс в списке - адрес инструкции
```json
{
  "opcode": "ldi",
  "arg1": "r1",
  "arg2": 2
}
```
где:
- `opcode` -- код операции
- `arg1` -- первый аргумент (может отсутствовать)
- `arg2` -- второй аргумент (может отсутствовать)

## Транслятор
Интерфейс командной строки: `translator.py <input_file> <target_file>`

Реализовано в модуле [translator.py](translator.py)

Этапы трансляции:
1. Проверка парности скобок
2. Трансформация текста программы в AST
3. Определение перменных и генерация машинного кода

На выходе из транслятора получаем файл, состоящий из двух секций
- `data` -- здесь описаны все константы
- `text` -- машинный код

## Модель процессора
Реализовано в модуле [machine.py](machine.py)

Схема:

![Изображение общей схемы](schema.png)

Управляющие сигналы:
- RD_INSTR - прочесть из памяти команд следующую команду
- LATCH_PC - защелкнуть значение PC
- PC_SET - выставить значение мультиплексора, который определяет следующее значение PC
  - 0 -- PC + 1
  - 1 -- значение из инструкции
- SEL_LEFT_ALU - выставить значение мультиплексора на левый вход АЛУ
  - 0 -- 0
  - 1 -- из регистра
- SEL_RIGHT_ALU - выставить значение мультиплексора на правый вход АЛУ
  - 0 -- 0
  - 1 -- из регистра
  - 2 -- immediate значение
- LATCH_REG -- защелкнуть значение в регистр
- LATCH_DATA_ADR -- защелкнуть значение регистра адреса
- SEL_OUT_MEM -- выставить значение мультиплексора откуда записывать данные (АЛУ или память)
- MEM_READ -- считать значение из памяти
- MEM_WRITE -- записать значние в память

Особенности работы модели:
- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключений:
  - EOFError -- если нет данных для чтения из порта ввода-вывода; 
  - StopIteration -- если выполнена инструкция halt.
- Управление симуляцией реализовано в функции simulate.

## Аппробация
- [Исходные коды тестовых программ](examples)
- [Результаты работы транслятора](code_out)
- [Входные данные для программ](test_input)

Пример работы модели:
```shell
> ./translator.py examples/helloworld.lisp code_out/helloworld.o

> cat examples/helloworld.lisp
(setq name "Hello, world!")
(print name)

> cat code_out/helloworld.o
{
    "data": [
        {
            "address": 2,
            "value": "Hello, world!"
        }
    ],
    "text": [
        {
            "opcode": "ldi",
            "arg1": "r1",
            "arg2": 2
        },
        {
            "opcode": "load",
            "arg1": "r0",
            "arg2": "r1"
        },
        {
            "opcode": "cmpi",
            "arg1": "r0",
            "arg2": 0
        },
        {
            "opcode": "jz",
            "arg1": 7
        },
        {
            "opcode": "sta",
            "arg1": 1,
            "arg2": "r0"
        },
        {
            "opcode": "addi",
            "arg1": "r1",
            "arg2": 1
        },
        {
            "opcode": "jmp",
            "arg1": 1
        },
        {
            "opcode": "hlt"
        }
    ]
}

> ./machine.py code_out/helloworld.o test_input/helloworld.txt
DEBUG:root:{TICK: 0, PC: 0, SP: 0x0, Z: True, ADDR: 0x2, VALUE: 72, rO: 0, r1: 0, r2: 0, r3: 0, r4: 0} ldi r1 2
DEBUG:root:{TICK: 1, PC: 1, SP: 0x0, Z: False, ADDR: 0x2, VALUE: 72, rO: 0, r1: 2, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 3, PC: 2, SP: 0x0, Z: False, ADDR: 0x2, VALUE: 72, rO: 72, r1: 2, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 4, PC: 3, SP: 0x0, Z: False, ADDR: 0x2, VALUE: 72, rO: 72, r1: 2, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 5, PC: 4, SP: 0x0, Z: False, ADDR: 0x2, VALUE: 72, rO: 72, r1: 2, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 7, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 72, r1: 2, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 8, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 72, r1: 3, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 9, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 72, r1: 3, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 11, PC: 2, SP: 0x0, Z: False, ADDR: 0x3, VALUE: 101, rO: 101, r1: 3, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 12, PC: 3, SP: 0x0, Z: False, ADDR: 0x3, VALUE: 101, rO: 101, r1: 3, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 13, PC: 4, SP: 0x0, Z: False, ADDR: 0x3, VALUE: 101, rO: 101, r1: 3, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 15, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 101, r1: 3, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 16, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 101, r1: 4, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 17, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 101, r1: 4, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 19, PC: 2, SP: 0x0, Z: False, ADDR: 0x4, VALUE: 108, rO: 108, r1: 4, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 20, PC: 3, SP: 0x0, Z: False, ADDR: 0x4, VALUE: 108, rO: 108, r1: 4, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 21, PC: 4, SP: 0x0, Z: False, ADDR: 0x4, VALUE: 108, rO: 108, r1: 4, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 23, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 4, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 24, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 5, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 25, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 5, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 27, PC: 2, SP: 0x0, Z: False, ADDR: 0x5, VALUE: 108, rO: 108, r1: 5, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 28, PC: 3, SP: 0x0, Z: False, ADDR: 0x5, VALUE: 108, rO: 108, r1: 5, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 29, PC: 4, SP: 0x0, Z: False, ADDR: 0x5, VALUE: 108, rO: 108, r1: 5, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 31, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 5, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 32, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 6, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 33, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 6, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 35, PC: 2, SP: 0x0, Z: False, ADDR: 0x6, VALUE: 111, rO: 111, r1: 6, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 36, PC: 3, SP: 0x0, Z: False, ADDR: 0x6, VALUE: 111, rO: 111, r1: 6, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 37, PC: 4, SP: 0x0, Z: False, ADDR: 0x6, VALUE: 111, rO: 111, r1: 6, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 39, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 111, r1: 6, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 40, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 111, r1: 7, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 41, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 111, r1: 7, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 43, PC: 2, SP: 0x0, Z: False, ADDR: 0x7, VALUE: 44, rO: 44, r1: 7, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 44, PC: 3, SP: 0x0, Z: False, ADDR: 0x7, VALUE: 44, rO: 44, r1: 7, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 45, PC: 4, SP: 0x0, Z: False, ADDR: 0x7, VALUE: 44, rO: 44, r1: 7, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 47, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 44, r1: 7, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 48, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 44, r1: 8, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 49, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 44, r1: 8, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 51, PC: 2, SP: 0x0, Z: False, ADDR: 0x8, VALUE: 32, rO: 32, r1: 8, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 52, PC: 3, SP: 0x0, Z: False, ADDR: 0x8, VALUE: 32, rO: 32, r1: 8, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 53, PC: 4, SP: 0x0, Z: False, ADDR: 0x8, VALUE: 32, rO: 32, r1: 8, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 55, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 32, r1: 8, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 56, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 32, r1: 9, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 57, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 32, r1: 9, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 59, PC: 2, SP: 0x0, Z: False, ADDR: 0x9, VALUE: 119, rO: 119, r1: 9, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 60, PC: 3, SP: 0x0, Z: False, ADDR: 0x9, VALUE: 119, rO: 119, r1: 9, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 61, PC: 4, SP: 0x0, Z: False, ADDR: 0x9, VALUE: 119, rO: 119, r1: 9, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 63, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 119, r1: 9, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 64, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 119, r1: 10, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 65, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 119, r1: 10, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 67, PC: 2, SP: 0x0, Z: False, ADDR: 0xa, VALUE: 111, rO: 111, r1: 10, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 68, PC: 3, SP: 0x0, Z: False, ADDR: 0xa, VALUE: 111, rO: 111, r1: 10, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 69, PC: 4, SP: 0x0, Z: False, ADDR: 0xa, VALUE: 111, rO: 111, r1: 10, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 71, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 111, r1: 10, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 72, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 111, r1: 11, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 73, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 111, r1: 11, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 75, PC: 2, SP: 0x0, Z: False, ADDR: 0xb, VALUE: 114, rO: 114, r1: 11, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 76, PC: 3, SP: 0x0, Z: False, ADDR: 0xb, VALUE: 114, rO: 114, r1: 11, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 77, PC: 4, SP: 0x0, Z: False, ADDR: 0xb, VALUE: 114, rO: 114, r1: 11, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 79, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 114, r1: 11, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 80, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 114, r1: 12, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 81, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 114, r1: 12, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 83, PC: 2, SP: 0x0, Z: False, ADDR: 0xc, VALUE: 108, rO: 108, r1: 12, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 84, PC: 3, SP: 0x0, Z: False, ADDR: 0xc, VALUE: 108, rO: 108, r1: 12, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 85, PC: 4, SP: 0x0, Z: False, ADDR: 0xc, VALUE: 108, rO: 108, r1: 12, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 87, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 12, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 88, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 13, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 89, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 108, r1: 13, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 91, PC: 2, SP: 0x0, Z: False, ADDR: 0xd, VALUE: 100, rO: 100, r1: 13, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 92, PC: 3, SP: 0x0, Z: False, ADDR: 0xd, VALUE: 100, rO: 100, r1: 13, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 93, PC: 4, SP: 0x0, Z: False, ADDR: 0xd, VALUE: 100, rO: 100, r1: 13, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 95, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 100, r1: 13, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 96, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 100, r1: 14, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 97, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 100, r1: 14, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 99, PC: 2, SP: 0x0, Z: False, ADDR: 0xe, VALUE: 33, rO: 33, r1: 14, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 100, PC: 3, SP: 0x0, Z: False, ADDR: 0xe, VALUE: 33, rO: 33, r1: 14, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 101, PC: 4, SP: 0x0, Z: False, ADDR: 0xe, VALUE: 33, rO: 33, r1: 14, r2: 0, r3: 0, r4: 0} sta 1 r0
DEBUG:root:{TICK: 103, PC: 5, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 33, r1: 14, r2: 0, r3: 0, r4: 0} addi r1 1
DEBUG:root:{TICK: 104, PC: 6, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 33, r1: 15, r2: 0, r3: 0, r4: 0} jmp 1 
DEBUG:root:{TICK: 105, PC: 1, SP: 0x0, Z: False, ADDR: 0x1, VALUE: 0, rO: 33, r1: 15, r2: 0, r3: 0, r4: 0} load r0 r1
DEBUG:root:{TICK: 107, PC: 2, SP: 0x0, Z: False, ADDR: 0xf, VALUE: 0, rO: 0, r1: 15, r2: 0, r3: 0, r4: 0} cmpi r0 0
DEBUG:root:{TICK: 108, PC: 3, SP: 0x0, Z: True, ADDR: 0xf, VALUE: 0, rO: 0, r1: 15, r2: 0, r3: 0, r4: 0} jz 7 
DEBUG:root:{TICK: 109, PC: 7, SP: 0x0, Z: True, ADDR: 0xf, VALUE: 0, rO: 0, r1: 15, r2: 0, r3: 0, r4: 0} hlt  
Hello, world!
instr_counter:  82 ticks: 109

Process finished with exit code 0
```

| ФИО           | алг.  | LoC (в строках) | code байт | code инстр | инстр. | такт. | вариант                                                                           |
|:--------------|:------|:----------------|:----------|:-----------|:-------|:------|:----------------------------------------------------------------------------------|
| Вахрушев Д.С. | hello | 2               | -         | 8          | 82     | 109   | `lisp` - `risc` - `harv` - `hw` - `instr` - `struct` - `stream` - `mem` - `prob1` |
| Вахрушев Д.С. | cat   | 3               | -         | 16         | 319    | 420   | `lisp` - `risc` - `harv` - `hw` - `instr` - `struct` - `stream` - `mem` - `prob1` |
| Вахрушев Д.С. | prob1 | 6               | -         | 52         | 24979  | 31404 | `lisp` - `risc` - `harv` - `hw` - `instr` - `struct` - `stream` - `mem` - `prob1` |




  
  


