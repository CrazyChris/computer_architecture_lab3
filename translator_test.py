"""
Тесты для транслятора
"""


import pytest
import translator


@pytest.mark.golden_test("test_data/parser/*.yml")
def test_parse(golden):
    assert translator.parse_program(golden["input"]) == golden.out["output"]


@pytest.mark.parametrize("text", ["(()", "(print))", "())", "(", ")",
                                  "(((", ")))))", "(setq name 22)()("])
def test_parse_with_wrong_braces(text):
    with pytest.raises(Exception, match="Braces are not balanced"):
        translator.parse_program(text)


@pytest.mark.parametrize("text", ["(print 12)", "(print \"name\")", "(print)"])
def test_print_not_variable(text):
    code = translator.parse_program(text)
    with pytest.raises(Exception):
        translator.main(code)
