#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=too-many-public-methods
# pylint: disable=unused-argument

"""
Транслятор MyLisp в машинный код
"""

import re
import sys
from enum import Enum
from isa import Opcode, write_code, REG_NUMBER


def parse_program(code):
    count = 0
    for c in code:
        if c == '(':
            count += 1
        elif c == ')':
            count -= 1
        if count < 0:
            break
    if count != 0:
        raise Exception("Braces are not balanced")

    def parse(text):
        tokens = []
        cur_token = ""
        is_string = False

        i = 0
        while i < len(text):
            char = text[i]

            if is_string and char == '"':
                cur_token += char
                tokens.append(cur_token)
                cur_token = ''
                is_string = False
            elif is_string:
                cur_token += char
            elif not is_string:
                if char == '"':
                    is_string = True
                    cur_token += char
                elif char == "(":
                    parsed, rest = parse(text[i + 1:])
                    tokens.append(parsed)
                    text = rest
                    i = 0
                    continue
                elif char == ")":
                    if cur_token != '':
                        tokens.append(cur_token)
                    return [tokens, text[i + 1:]]
                elif char in (" ", "\n"):
                    if cur_token != '':
                        tokens.append(cur_token)
                    cur_token = ''
                else:
                    cur_token += char

            i += 1

        return [tokens, '']

    res, _ = parse(code)
    return res


class RegisterScope:
    """
    Класс, ответственный за распределение регистров
    """
    def __init__(self):
        self.registers = {}
        for i in range(REG_NUMBER):
            reg_name = 'r' + str(i)
            self.registers[reg_name] = False

    def take_reg(self):
        for reg, used in self.registers.items():
            if not used:
                self.registers[reg] = True
                return reg
        raise Exception("No registers available")

    def free_reg(self, name):
        self.registers[name] = False


class Type(str, Enum):
    """Поддерживаемые типы данных"""
    STRING = 'str'
    CHAR = 'char'
    INT = 'int'


class Location(str, Enum):
    """Расположение результата выражения"""
    MEMORY = "mem"
    REGISTER = "reg"


class Compiler:
    """Транслятор MyLisp'а"""

    def __init__(self):
        self.strings = {}

        self.variables = {}

        self.input_device = 0
        self.output_device = 1

        self.stack_pointer = 'sp'

        self.memory_pointer = 2
        self.next_command_id = 0

        self.registers = RegisterScope()

        self.outBuffer = []
        self.primitive_functions = {
            '+': self.create_binary_operation(Opcode.ADD),
            '-': self.create_binary_operation(Opcode.SUB),
            'div': self.create_binary_operation(Opcode.DIV),
            'mod': self.create_binary_operation(Opcode.MOD),
            '=': self.create_binary_operation(Opcode.EQ),
            '>': self.create_binary_operation(Opcode.GT),
            '<': self.create_binary_operation(Opcode.LT),
            '>=': self.create_binary_operation(Opcode.GE),
            'or': self.create_binary_operation(Opcode.OR),
            'print': self.compile_print,
            'read': self.compile_input,
            'setq': self.compile_setq,
            'if': self.compile_if,
            'do': self.compile_loop
        }
        self.function_scope = {}

    def load_variable(self, adr):
        dest = self.registers.take_reg()
        self.emit(Opcode.LDA, dest, adr)
        return dest

    def create_binary_operation(self, opcode: Opcode):

        def binary_func(args):
            arg1, arg2 = args

            adr1 = self.compile_expression(arg1)
            adr2 = self.compile_expression(arg2)

            if adr1['location'] is Location.MEMORY:
                arg1 = self.load_variable(adr1['adr'])
            else:
                arg1 = adr1['adr']
            if adr2['location'] is Location.MEMORY:
                arg2 = self.load_variable(adr2['adr'])
            else:
                arg2 = adr2['adr']

            self.emit(opcode, arg1, arg2)
            self.registers.free_reg(arg2)

            return {
                'type': Type.INT,
                'adr': arg1,
                'location': Location.REGISTER
            }

        return binary_func

    def allocate_string(self, value):
        self.strings[value] = self.memory_pointer
        self.memory_pointer += len(value) - 2 + 1
        return self.strings[value]

    def create_variable(self, name, var_type, address):
        self.variables[name] = {
            'type': var_type,
            'adr': address,
        }
        return self.variables[name]

    def emit(self, opcode, arg1=None, arg2=None):
        cmd = {
            'opcode': opcode.value,
        }
        if arg1 is not None:
            cmd['arg1'] = arg1
        if arg2 is not None:
            cmd['arg2'] = arg2
        self.outBuffer.append(cmd)
        self.next_command_id = self.next_command_id + 1
        return self.next_command_id - 1

    def compile_setq(self, args):
        name = args[0]
        value = args[1]

        adr = self.compile_expression(value)

        if adr['location'] is Location.MEMORY:
            self.create_variable(name, adr['type'], adr['adr'])
            return

        if name not in self.variables:
            self.memory_pointer += 1
            self.create_variable(name, adr['type'], self.memory_pointer)
            self.emit(Opcode.STA, self.memory_pointer, adr['adr'])
        else:
            self.emit(Opcode.STA, self.variables[name]['adr'], adr['adr'])
        self.registers.free_reg(adr['adr'])

    def resolve_condition(self, condition):
        res = self.compile_expression(condition)
        if res['location'] is Location.MEMORY:
            res_reg = self.load_variable(res['adr'])
        else:
            res_reg = res['adr']
        return res_reg

    def compile_if(self, args):
        if len(args) < 2:
            raise Exception("No if body provided")
        condition = args[0]
        action = args[1]
        alternative = args[2] if len(args) > 2 else None

        res_reg = self.resolve_condition(condition)

        self.emit(Opcode.CMPI, res_reg, 0)
        self.registers.free_reg(res_reg)
        temp = self.emit(Opcode.JZ)
        self.compile_expression(action)
        action_end = len(self.outBuffer)

        if alternative is not None:
            alt_start = self.emit(Opcode.JMP)
            action_end += 1
            self.compile_expression(alternative)
            self.outBuffer[alt_start]['arg1'] = len(self.outBuffer)

        self.outBuffer[temp]['arg1'] = action_end

    def compile_loop(self, args):
        if len(args) < 2:
            raise Exception("Too few arguments for a loop")

        variable_def = args[0]
        exit_condition = args[1]
        body = args[2]

        variables = {}

        for var in variable_def:
            adr = self.compile_expression(var[1])
            if adr['location'] is Location.REGISTER:
                self.memory_pointer += 1
                self.emit(Opcode.STA, self.memory_pointer, adr['adr'])
                self.create_variable(var[0], Type.STRING, self.memory_pointer)
                self.registers.free_reg(adr['adr'])
            else:
                self.create_variable(var[0], Type.STRING, adr['adr'])
            variables[var[0]] = var[2]

        on_condition = len(self.outBuffer)

        res_reg = self.resolve_condition(exit_condition)

        self.emit(Opcode.CMPI, res_reg, 0)
        self.registers.free_reg(res_reg)
        jz = self.emit(Opcode.JNZ)

        self.compile_expression(body)
        for name, update in variables.items():
            res = self.compile_expression(update)
            self.emit(Opcode.STA, self.variables[name]['adr'], res['adr'])
        self.emit(Opcode.JMP, on_condition)
        self.outBuffer[jz]['arg1'] = len(self.outBuffer)

    def gen_push(self, reg):
        self.emit(Opcode.ADDI, self.stack_pointer, -1)
        return self.emit(Opcode.STORE, self.stack_pointer, reg)

    def gen_pop(self, reg):
        com_id = self.emit(Opcode.LOAD, reg, self.stack_pointer)
        self.emit(Opcode.ADDI, self.stack_pointer, 1)
        return com_id

    def print_int_helper(self, reg1, reg2, divisor):
        start_id = self.emit(Opcode.CMPI, reg1, 0)
        jz_id = self.emit(Opcode.JZ)
        self.emit(Opcode.MOD, reg1, divisor)
        self.gen_push(reg1)
        self.emit(Opcode.DIV, reg2, divisor)
        self.emit(Opcode.MOV, reg1, reg2)
        end_id = self.emit(Opcode.JMP, start_id)
        self.outBuffer[jz_id]['arg1'] = end_id + 1
        self.registers.free_reg(divisor)

        start_id = self.gen_pop(reg1)
        self.emit(Opcode.CMPI, reg1, -1)
        jz_id = self.emit(Opcode.JZ)
        self.emit(Opcode.ADDI, reg1, 0x30)
        self.emit(Opcode.STA, self.output_device, reg1)
        end_id = self.emit(Opcode.JMP, start_id)
        self.outBuffer[jz_id]['arg1'] = end_id + 1

        self.registers.free_reg(reg1)
        self.registers.free_reg(reg2)

    def generate_print_int(self, variable):
        reg1 = self.registers.take_reg()
        reg2 = self.registers.take_reg()
        divisor = self.registers.take_reg()
        self.emit(Opcode.LDI, divisor, 10)
        self.emit(Opcode.LDI, reg1, -1)
        self.gen_push(reg1)
        self.emit(Opcode.LDA, reg1, variable['adr'])
        self.emit(Opcode.MOV, reg2, reg1)

        self.print_int_helper(reg1, reg2, divisor)

    def generate_print_int_in_reg(self, reg):
        reg1 = self.registers.take_reg()
        reg2 = self.registers.take_reg()
        divisor = self.registers.take_reg()
        self.emit(Opcode.LDI, divisor, 10)
        self.emit(Opcode.LDI, reg1, -1)
        self.gen_push(reg1)
        self.emit(Opcode.MOV, reg1, reg)
        self.emit(Opcode.MOV, reg2, reg1)
        self.registers.free_reg(reg)

        self.print_int_helper(reg1, reg2, divisor)

        self.registers.free_reg(reg1)
        self.registers.free_reg(reg2)

    def generate_print_string(self, variable):
        reg = self.registers.take_reg()
        counter = self.registers.take_reg()
        self.emit(Opcode.LDI, counter, variable['adr'])
        self.print_string_helper(reg, counter)

    def generate_print_string_in_reg(self, regvar):
        reg = self.registers.take_reg()
        counter = regvar
        self.print_string_helper(reg, counter)

    def print_string_helper(self, reg, counter):
        start_id = self.emit(Opcode.LOAD, reg, counter)
        self.emit(Opcode.CMPI, reg, 0)
        jz_id = self.emit(Opcode.JZ)
        self.emit(Opcode.STA, 0x1, reg)
        self.emit(Opcode.ADDI, counter, 1)
        end_id = self.emit(Opcode.JMP, start_id)
        self.outBuffer[jz_id]['arg1'] = end_id + 1
        self.registers.free_reg(reg)
        self.registers.free_reg(counter)

    def compile_print(self, args):
        if args is None or len(args) == 0:
            raise Exception("Too gew arguments for print()")

        for arg in args:
            var = self.compile_expression(arg)

            if var['location'] is Location.MEMORY:
                if var['type'] is Type.CHAR:
                    reg = self.load_variable(var['adr'])
                    self.emit(Opcode.STA, self.output_device, reg)
                    self.registers.free_reg(reg)
                elif var['type'] is Type.STRING:
                    self.generate_print_string(var)
                else:
                    self.generate_print_int(var)
            else:
                if var['type'] is Type.CHAR:
                    self.emit(Opcode.STA, self.output_device, var['adr'])
                elif var['type'] is Type.INT:
                    self.generate_print_int_in_reg(var['adr'])
                else:
                    self.generate_print_string_in_reg(var['adr'])

    def compile_input(self, args):
        dest = self.registers.take_reg()
        self.emit(Opcode.LDA, dest, self.input_device)
        return {
            'type': Type.CHAR,
            'adr': dest,
            'location': Location.REGISTER
        }

    def compile_number(self, value):
        """
        Return address where the number is stored or allocate new number
        :param value: number to resolve
        :return: address where the number is stored
        """
        reg = self.registers.take_reg()
        self.emit(Opcode.LDI, reg, value)

        return {
            'type': Type.INT,
            'adr': reg,
            'location': Location.REGISTER
        }

    # returns address where the string is stored
    def compile_string(self, value):
        if value in self.strings:
            src = self.strings[value]
        else:
            src = self.allocate_string(value)

        return {
            'type': Type.STRING,
            'adr': src,
            'location': Location.MEMORY
        }

    def compile_variable(self, var):
        if var in self.variables:
            data = self.variables[var]
        else:
            raise Exception(f"No variable with name {var} found")

        return {
            'type': data['type'],
            'adr': data['adr'],
            'location': Location.MEMORY
        }

    def compile_expression(self, arg):
        if isinstance(arg, list):
            return self.compile_call(arg[0], arg[1:])

        if str(arg).isnumeric():
            return self.compile_number(int(arg))

        if re.fullmatch('\"(\\.|[^\"])*\"', str(arg)):
            return self.compile_string(arg)

        return self.compile_variable(arg)

    def compile_call(self, fun, args):
        if str(fun) in self.primitive_functions:
            return self.primitive_functions[fun](args)

        res = self.compile_expression(fun)
        for arg in args:
            self.compile_expression(arg)
        return res

    def compile_program(self, code):
        for expr in code:
            self.compile_expression(expr)
        self.emit(Opcode.HLT)

    def get_output(self):
        out = {'data': [], 'text': self.outBuffer}
        for val, addr in self.strings.items():
            out['data'].append({'address': addr, 'value': val[1:-1]})
        return out


def main(args):
    input_file, output_file = args[0], args[1]
    with open(input_file, encoding="utf-8") as file:
        code = file.read()
    tokens = parse_program(code)
    compiler = Compiler()
    compiler.compile_program(tokens)
    write_code(output_file, compiler.get_output())


if __name__ == '__main__':
    main(sys.argv[1:])
