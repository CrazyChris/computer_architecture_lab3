#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=too-few-public-methods
# pylint: disable=consider-using-f-string

"""
Модель процессора, позволяющая выполнить странслированные программы на языке MyLisp
"""

import logging
import sys
from enum import Enum

from isa import Opcode, read_code, REG_NUMBER


class Device:
    """
    Устройство воода-вывода
    """
    def __init__(self):
        self.output_buffer: list[str] = []
        self.input_buffer: list[str] = []

    def read(self) -> int:
        if len(self.input_buffer) <= 0:
            raise EOFError("EOF reached")
        return ord(self.input_buffer.pop(0))

    def write(self, value: int):
        self.output_buffer.append(chr(value))

    def mount(self, input_buffer):
        self.input_buffer = input_buffer


class ALU:
    """
    АЛУ, производящее вычисления. Имеет левый и правый входы и выходное значение
    """
    def __init__(self):
        self.left = 0
        self.right = 0
        self.out = 0
        self.zero = True

        self.binary_functions = {
            Opcode.DIV: lambda: self.left // self.right,
            Opcode.MOD: lambda: self.left % self.right,
            Opcode.MUL: lambda: self.left * self.right,
            Opcode.ADD: lambda: self.left + self.right,
            Opcode.SUB: lambda: self.left - self.right,
            Opcode.EQ: lambda: int(self.left == self.right),
            Opcode.OR: lambda: int(self.left or self.right),
            Opcode.GT: lambda: int(self.left > self.right),
            Opcode.LT: lambda: int(self.left < self.right),
            Opcode.GE: lambda: int(self.left >= self.right)
        }

    def conduct_operation(self, opcode):
        self.out = self.binary_functions[opcode]()
        self.zero = self.out == 0


class DataPath:
    """Тракт данных (пассивный), включая: ввод/вывод, память и арифметику."""

    def __init__(self, data_memory_size, input_buffer):
        assert data_memory_size > 0, "Data_memory size should be non-zero"
        self.data_memory_size = data_memory_size
        self.data_memory = [0] * data_memory_size
        self.registers = {f'r{i}': 0 for i in range(REG_NUMBER)}
        self.registers['sp'] = 0

        self.alu = ALU()

        self.device = Device()
        self.device.mount(input_buffer)
        self.input_adr = 0
        self.output_adr = 1

        self.data_address = 2

        self.PC_SEL_MUX = 0
        self.ALU_LEFT_MUX = 0
        self.ALU_RIGHT_MUX = 0
        self.REG_WRITE_MUX = 0

        self.imm = 0
        self.rs1 = 0
        self.rs2 = 0

    def is_zero(self):
        return self.alu.zero

    def is_reg_zero(self, reg):
        return self.registers[reg] == 0

    def load_program_data(self, data):
        for var in data:
            addr = var['address']
            value = var['value']
            if isinstance(value, int):
                self.data_memory[addr] = value
            elif isinstance(value, str):
                for char in value:
                    self.data_memory[addr] = ord(char)
                    addr += 1
                self.data_memory[addr] = 0
            else:
                raise Exception("Unknown data type loaded to memory")

    def latch_data_adr(self):
        address = self.alu.out
        assert address < self.data_memory_size, f"out of memory: {address}"
        assert address >= 0, f"invalid address: {address}"
        self.data_address = address

    def latch_register(self):
        reg = self.rs1
        if self.REG_WRITE_MUX == Signal.SET_OUT_ALU:
            self.registers[reg] = self.alu.out
        else:
            self.registers[reg] = self.memory_read()
        if reg == 'sp':
            self.registers[reg] %= self.data_memory_size

    def memory_read(self):
        if self.data_address == self.input_adr:
            return self.device.read()

        return self.data_memory[self.data_address]

    def memory_write(self):
        value = self.registers[self.rs2]
        if self.data_address == self.output_adr:
            self.device.write(value)
        else:
            self.data_memory[self.data_address] = value

    def conduct_alu_operation(self, opcode):
        if self.ALU_LEFT_MUX is Signal.SET_ALU_REG:
            self.alu.left = self.registers[self.rs1]
        elif self.ALU_LEFT_MUX is Signal.SET_ALU_ZERO:
            self.alu.left = 0

        if self.ALU_RIGHT_MUX is Signal.SET_ALU_REG:
            self.alu.right = self.registers[self.rs2]
        elif self.ALU_RIGHT_MUX is Signal.SET_ALU_IMM:
            self.alu.right = self.imm
        elif self.ALU_RIGHT_MUX is Signal.SET_ALU_ZERO:
            self.alu.right = 0

        self.alu.conduct_operation(opcode)


class Signal(int, Enum):
    """
    Signal that Control Unit sends to MUXes
    """

    SET_NEXT_PC = 0
    SET_CUSTOM_PC = 1

    SET_OUT_MEM = 1
    SET_OUT_ALU = 0

    SET_ALU_ZERO = 0
    SET_ALU_REG = 1
    SET_ALU_IMM = 2


class ControlUnit:
    """
    Блок управления процессора. Выполняет декодирование инструкций и
    управляет состоянием процессора, включая обработку данных (DataPath)."""

    def __init__(self, program, data_path):
        self.program = program
        self.data_path: DataPath = data_path
        self.program_counter = 0
        self._tick = 0

        self.instr = None

    def tick(self):
        self._tick += 1

    def current_tick(self):
        return self._tick

    def latch_program_counter(self):
        if self.data_path.PC_SEL_MUX is Signal.SET_NEXT_PC:
            self.program_counter += 1
        else:
            instr = self.program[self.program_counter]
            if instr['opcode'] in [Opcode.JMP, Opcode.JZ, Opcode.JNZ]:
                self.program_counter = instr['arg1']
            else:
                raise Exception("Internal error")

    def read_instruction(self):
        self.instr = self.program[self.program_counter]
        opcode = self.instr["opcode"]
        self.data_path.rs1 = self.instr.get("arg1")
        self.data_path.rs2 = self.instr.get("arg2")
        if opcode in [Opcode.LDA, Opcode.LDI, Opcode.ADDI, Opcode.CMPI]:
            self.data_path.imm = self.instr["arg2"]
        elif opcode in [Opcode.JMP, Opcode.JNZ, Opcode.JZ, Opcode.STA]:
            self.data_path.imm = self.instr["arg1"]

    def execute_load(self, opcode):
        self.data_path.PC_SEL_MUX = Signal.SET_NEXT_PC
        self.data_path.ALU_LEFT_MUX = Signal.SET_ALU_ZERO
        if opcode is Opcode.LDA:
            self.data_path.ALU_RIGHT_MUX = Signal.SET_ALU_IMM
            self.data_path.conduct_alu_operation(Opcode.ADD)
            self.data_path.latch_data_adr()
            self.tick()

            self.data_path.REG_WRITE_MUX = Signal.SET_OUT_MEM
            self.data_path.latch_register()
            self.tick()
        elif opcode is Opcode.LDI:
            self.data_path.ALU_RIGHT_MUX = Signal.SET_ALU_IMM
            self.data_path.REG_WRITE_MUX = Signal.SET_OUT_ALU
            self.data_path.conduct_alu_operation(Opcode.ADD)
            self.data_path.latch_register()
            self.tick()
        elif opcode is Opcode.LOAD:
            self.data_path.ALU_RIGHT_MUX = Signal.SET_ALU_REG
            self.data_path.conduct_alu_operation(Opcode.ADD)
            self.data_path.latch_data_adr()
            self.tick()

            self.data_path.REG_WRITE_MUX = Signal.SET_OUT_MEM
            self.data_path.latch_register()
            self.tick()

    def execute_store(self, opcode):
        self.data_path.PC_SEL_MUX = Signal.SET_NEXT_PC

        if opcode is Opcode.STORE:
            self.data_path.ALU_LEFT_MUX = Signal.SET_ALU_REG
            self.data_path.ALU_RIGHT_MUX = Signal.SET_ALU_ZERO
            self.data_path.conduct_alu_operation(Opcode.ADD)
            self.data_path.latch_data_adr()
            self.tick()
        elif opcode is Opcode.STA:
            self.data_path.ALU_LEFT_MUX = Signal.SET_ALU_ZERO
            self.data_path.ALU_RIGHT_MUX = Signal.SET_ALU_IMM
            self.data_path.conduct_alu_operation(Opcode.ADD)
            self.data_path.latch_data_adr()
            self.tick()

        self.data_path.memory_write()
        self.tick()

    def execute_jump(self, opcode):
        if opcode is Opcode.JMP:
            self.data_path.PC_SEL_MUX = Signal.SET_CUSTOM_PC
        elif opcode is Opcode.JZ:
            if self.data_path.is_zero():
                self.data_path.PC_SEL_MUX = Signal.SET_CUSTOM_PC
            else:
                self.data_path.PC_SEL_MUX = Signal.SET_NEXT_PC
        elif opcode is Opcode.JNZ:
            if self.data_path.is_zero():
                self.data_path.PC_SEL_MUX = Signal.SET_NEXT_PC
            else:
                self.data_path.PC_SEL_MUX = Signal.SET_CUSTOM_PC

        self.tick()

    def execute_immediate_alu_operation(self, opcode):
        self.data_path.PC_SEL_MUX = Signal.SET_NEXT_PC
        self.data_path.ALU_LEFT_MUX = Signal.SET_ALU_REG
        self.data_path.ALU_RIGHT_MUX = Signal.SET_ALU_IMM
        if opcode is Opcode.ADDI:
            self.data_path.REG_WRITE_MUX = Signal.SET_OUT_ALU
            self.data_path.conduct_alu_operation(Opcode.ADD)
            self.data_path.latch_register()
        elif opcode is Opcode.CMPI:
            self.data_path.conduct_alu_operation(Opcode.SUB)
        self.tick()

    def decode_and_execute_instruction(self):
        self.read_instruction()
        opcode = self.instr["opcode"]

        if opcode is Opcode.HLT:
            raise StopIteration()
        if opcode in [Opcode.LDA, Opcode.LDI, Opcode.LOAD]:
            self.execute_load(opcode)
        elif opcode in [Opcode.STORE, Opcode.STA]:
            self.execute_store(opcode)
        elif opcode is Opcode.MOV:
            self.data_path.PC_SEL_MUX = Signal.SET_NEXT_PC
            self.data_path.ALU_LEFT_MUX = Signal.SET_ALU_ZERO
            self.data_path.ALU_RIGHT_MUX = Signal.SET_ALU_REG
            self.data_path.REG_WRITE_MUX = Signal.SET_OUT_ALU
            self.data_path.conduct_alu_operation(Opcode.ADD)
            self.data_path.latch_register()
            self.tick()
        elif opcode in [Opcode.ADDI, Opcode.CMPI]:
            self.execute_immediate_alu_operation(opcode)
        elif opcode in [Opcode.JMP, Opcode.JZ, Opcode.JNZ]:
            self.execute_jump(opcode)
        elif opcode in self.data_path.alu.binary_functions:
            self.data_path.PC_SEL_MUX = Signal.SET_NEXT_PC
            self.data_path.ALU_LEFT_MUX = Signal.SET_ALU_REG
            self.data_path.ALU_RIGHT_MUX = Signal.SET_ALU_REG
            self.data_path.REG_WRITE_MUX = Signal.SET_OUT_ALU
            self.data_path.conduct_alu_operation(opcode)
            self.data_path.latch_register()
            self.tick()

        self.latch_program_counter()

    def __repr__(self):
        state = "{{TICK: {}, PC: {}, SP: {}, Z: {}, ADDR: {}, VALUE: {}, " \
                "rO: {}, r1: {}, r2: {}, r3: {}, r4: {}}}".format(
                    self._tick,
                    self.program_counter,
                    hex(self.data_path.registers['sp']),
                    self.data_path.is_zero(),
                    hex(self.data_path.data_address),
                    self.data_path.data_memory[self.data_path.data_address],
                    self.data_path.registers['r0'],
                    self.data_path.registers['r1'],
                    self.data_path.registers['r2'],
                    self.data_path.registers['r3'],
                    self.data_path.registers['r4'],
                )

        instr = self.program[self.program_counter]
        opcode = instr["opcode"]
        arg1 = instr.get("arg1", "")
        arg2 = instr.get("arg2", "")
        action = "{} {} {}".format(opcode, arg1, arg2)

        return "{} {}".format(state, action)


def simulation(code, input_tokens, data_memory_size, limit):
    data_path = DataPath(data_memory_size, input_tokens)
    data_path.load_program_data(code['data'])
    control_unit = ControlUnit(code['text'], data_path)
    instr_counter = 0

    try:
        logging.debug('%s', control_unit)
        while True:
            assert limit > instr_counter, "too long execution, increase limit!"
            control_unit.decode_and_execute_instruction()
            instr_counter += 1
            logging.debug('%s', control_unit)
    except EOFError:
        logging.warning("input buffer is empty")
    except StopIteration:
        pass

    return ''.join(data_path.device.output_buffer), instr_counter, control_unit.current_tick()


def main(args):
    assert len(args) == 2, "Wrong arguments: machine.py <code_file> <input_file>"
    code_file, input_file = args

    code = read_code(code_file)
    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        input_token = []
        for char in input_text:
            input_token.append(char)

    output, instr_counter, ticks = simulation(code,
                                              input_tokens=input_token,
                                              data_memory_size=100, limit=50000)

    print(''.join(output))
    logging.info("instr_counter: %d ticks: %d", instr_counter, ticks)
    return output


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
