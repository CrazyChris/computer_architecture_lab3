"""
Типы данных для представления и сериализации/десериализации машинного кода.
"""

import json
from collections import namedtuple
from enum import Enum

REG_NUMBER = 10


class Opcode(str, Enum):
    """Opcode для ISA."""
    LOAD = 'load'
    LDI = 'ldi'
    LDA = 'lda'
    STORE = 'store'
    STA = 'sta'
    MOV = 'mov'

    ADDI = 'addi'
    CMPI = 'cmpi'
    ADD = 'add'
    SUB = 'sub'
    MUL = 'mul'
    DIV = 'div'
    MOD = 'mod'

    OR = 'or'
    EQ = 'eq'
    GT = 'gt'
    LT = 'lt'
    GE = 'ge'

    HLT = 'hlt'
    JZ = 'jz'
    JNZ = 'jnz'
    JMP = 'jmp'


class Term(namedtuple('Term', 'line pos symbol')):
    """Описание выражения из исходного текста программы."""


def write_code(filename, code):
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))


def read_code(filename):
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())

    for instr in code['text']:
        # Конвертация строки в Opcode
        instr['opcode'] = Opcode(instr['opcode'])
        # Конвертация списка из term в класс Term
        if 'term' in instr:
            instr['term'] = Term(
                instr['term'][0], instr['term'][1], instr['term'][2])

    return code
