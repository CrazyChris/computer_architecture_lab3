(setq res 0)
(do ((cnt 0 (+ cnt 1)))
    (>= cnt 1000)
    (if (or (= (mod cnt 3) 0) (= (mod cnt 5) 0))
    (setq res (+ res cnt))))
(print res)

